<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @implments ImporterInterface
 */
abstract class Importer implements ImporterInterface
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $dataMap;

    /**
     * @var array
     */
    protected $forcedData;

    public function __construct(array $data, array $forcedData = [])
    {
        $this->data = $data;
        $this->forcedData = $forcedData;
    }

    public function getDataMap(): array
    {
        return $this->dataMap;
    }

    public function assignMappedData(Model $model, array $data = [], array $dataMap = []): void
    {
        $data = (empty($data)) ? $this->data : $data;
        $dataMap = (empty($dataMap)) ? $this->dataMap : $dataMap;

        $this->processData($model, $data, collect($dataMap));
    }

    public function assignForcedData(Model $model): void
    {
        foreach ($this->forcedData as $key => $value) {
            $model->{$key} = $value;
        }
    }

    public function getIncomingData(): array
    {
        return $this->data;
    }

    abstract public function transform(): Model;

    /**
     * Recursively loop over the mapped data and build our Model
     *
     * @param Model $model
     * @param array $data
     * @param Collection $map
     * @return void
     */
    private function processData($model, $data, $map)
    {
        $map->recursive()->each(function ($item, $key) use ($model, $data, $map) {
            if ($item instanceof Collection) {
                $this->processData($model, $data[$key], $item);
            } elseif (isset($data[$key])) {
                $model->{$item} = $data[$key];
            }
        });
    }
}
