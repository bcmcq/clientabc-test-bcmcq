<?php

namespace App\Imports\ClientABC;

use App\Models\Order;
use App\Imports\Importer;

/**
 * Transforms an incoming order from ClientABC into a eloquent model representation
 */
class OrderTransformer extends Importer
{
    protected $dataMap = [
        'recordId' => 'externalKey',
        'routingInstructions' => 'retailerCarrierServiceCode',
        'salesChannel' => 'salesChannel',
        'routingInstructions' => 'routingInstructions',
        'documentNumber' => 'customerPONumber',
        'shipAccountNo' => 'accountNumber',
        'documentAmount' => 'freightAmount',
        'carrierCode' => 'carrierCode',
        'partner' => 'partner',
        'shipDate' => 'shipEndDate',
        'poDate' => 'orderDate',
        'receiveDate' => 'externalReceivedDate',
        'documentDate' => 'dateCreated',
        'shipToAddress' => [
            'address1' => 'shippingAddress1',
            'address2' => 'shippingAddress2',
            'city' => 'shippingCity',
            'state' => 'shippingState',
            'companyName1' => 'deliveryName',
            'country' => 'shippingCountry',
            'postalCode' => 'shippingPostalCode',
            'locationNumber' => 'deliveryLocationCode',
        ],
        'billToAddress' => [
            'address1' => 'billingAddress1',
            'address2' => 'billingAddress2',
            'city' => 'billingCity',
            'state' => 'billingState',
            'country' => 'billingCountry',
            'postalCode' => 'billingPostalCode',
        ]
    ];

    public function __construct(array $data, array $forcedData = [])
    {
        parent::__construct($data, $forcedData);
    }

    public function transform(): Order
    {
        $order = Order::firstOrNew(['externalKey' => $this->data['recordId']]);

        $this->standardizeOrderData();
        $this->assignMappedData($order);
        $this->assignForcedData($order);

        $order->save();

        return $order;
    }

    /**
     * Set the shipToAddress & billToAddress on the incoming data when missing.
     * Data is derived from the addresses array based on address > type (BT, ST)
     *
     * @return void
     */
    public function standardizeOrderData()
    {
        if (!isset($this->data['shipToAddress']) || !isset($this->data['billToAddress'])) {
            foreach ($this->data['addresses'] as $address) {
                if ($address['type'] === 'BT') {
                    $this->data['billToAddress'] = $address;
                } elseif ($address['type'] === 'ST') {
                    $this->data['shipToAddress'] = $address;
                }
            }
        }
    }
}
