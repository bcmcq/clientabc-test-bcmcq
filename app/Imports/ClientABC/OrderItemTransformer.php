<?php

namespace App\Imports\ClientABC;

use App\Imports\Importer;
use App\Models\OrderItem;

/**
 * Transforms an incoming order item from ClientABC into a eloquent model representation
 */
class OrderItemTransformer extends Importer
{
    protected $dataMap = [
        'vendorItem' => 'sku',
        'lineNo' => 'lineItemNo',
        'unitMeasure' => 'UoM',
        'qtyOrder' => 'orderQuantity',
        'unitPrice' => 'unitPrice'
    ];

    public function __construct(array $data, array $forcedData = [])
    {
        parent::__construct($data, $forcedData);
    }

    public function transform(): OrderItem
    {
        $orderItem = OrderItem::firstOrNew([
                    'order_key' => $this->forcedData['order_key'],
                    'sku' => $this->data['vendorItem'],
                    'lineItemNo' => $this->data['lineNo']
                ]);

        $this->assignMappedData($orderItem);
        $this->assignForcedData($orderItem);

        $orderItem->save();

        return $orderItem;
    }
}
