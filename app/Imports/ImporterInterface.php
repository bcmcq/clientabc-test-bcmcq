<?php

namespace App\Imports;

use App\Imports\Importer;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface ImporterInterface
 *
 * Transforms data between two systems.
 * Incoming data, generally from 3rd party API's that are mapped to internal models.
 */
interface ImporterInterface
{
    /**
     * New instance of ImportInterface
     *
     * @param array $data               Data to be transformed into a model representation for saving to the database.
     * @param array $forcedData         Data to be persisted to the resulting model representation, this is used for
     *                                  relational id's, etc.
     */
    public function __construct(array $data, array $forcedData = []);

    /**
     * Data map between two systems
     *
     * @return array The incoming data keys that map to the model representation resulting from transformation.
     */
    public function getDataMap(): array;

    /**
     * Incoming data to be transformed
     *
     * @return array The incoming data that is to be transformed into an eloquent model.
     */
    public function getIncomingData(): array;

    /**
     * Loops the dataMap and moves values from the incoming data to the model representation.
     *
     * @param Model $model          Instance of the model in question.
     * @param array $incomingData   Data to be transformed into eloquent model representation.
     * @param array $dataMap        Data as it maps between the incoming and eloquent model.
     * @return void
     */
    public function assignMappedData(Model $model, array $incomingData, array $dataMap): void;

    /**
     * Loops the incoming pre assigned data and moves values to the model representation.
     *
     * @param Model $model
     * @return void
     */
    public function assignForcedData(Model $model): void;

    /**
     * Runs the transformation process that maps all needed data.
     *
     * @return Model Instance of the model in question.
     */
    public function transform(): Model;
}
