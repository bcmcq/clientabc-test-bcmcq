<?php

namespace App\Models;

use App\Observers\OrderItemObserver;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\RequiresValidation;

class OrderItem extends Model
{
    use RequiresValidation;

    /**
     * Setup Observers on Boot.
     */
    protected static function boot()
    {
        parent::boot();
        parent::observe(new OrderItemObserver());
    }

    /**
     * @var array
     */
    protected $appends = [
        'externalKey',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'order',
        'sku',
        'lineItemNo',
        'UoM',
        'orderQuantity',
        'unitPrice',
        'grossPrice',
        'externalOrderId',
        'externalKey',
        'lastModified'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'externalOrderId' => 'integer',
        'orderQuantity' => 'integer',
        'unitPrice' => 'float',
        'grossPrice' => 'float',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'order_key',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    public $searchable = [
        'sku'
    ];

    /**
     * @return array
     */
    public $rules = [
        'sku' => 'required|max:15',
        'lineItemNo' => 'required|max:50',
        'UoM' => 'required|max:50',
        'orderQuantity' => 'required|max:10',
        'unitPrice' => 'sometimes|max:10',
        'grossPrice' => 'sometimes|max:10',
        'externalOrderId' => 'sometimes|max:15',
        'externalKey' => 'sometimes|max:25'
    ];

    /**
     * @return string
     */
    public function getExternalKeyAttribute()
    {
        return "{$this->externalOrderId}/{$this->lineItemNo}";
    }

    /**************************************************************************
     * Relationships
     **************************************************************************/

    /**
     * @return Collection App\Models\Order
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_key', 'key');
    }
}
