<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use App\Observers\OrderObserver;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\RequiresValidation;

class Order extends Model
{
    use RequiresValidation;

    /**
     * Setup Observers on Boot.
     */
    protected static function boot()
    {
        parent::boot();
        parent::observe(new OrderObserver());
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'key';
    }

    /**
     * @var array
     */
    protected $fillable = [
        'retailerCarrierServiceCode',
        'salesChannel',
        'routingInstructions',
        'customerPONumber',
        'accountNumber',
        'carrierCode',
        'partner',
        'deliveryLocationCode',
        'freightAmount',
        'deliveryName',
        'shippingAddress1',
        'shippingAddress2',
        'shippingCity',
        'shippingState',
        'shippingPostalCode',
        'shippingCountry',
        'isTaxable',
        'dateCreated',
        'orderDate',
        'lastModified',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'isTaxable' => 'boolean',
        'freightAmount' => 'float',
    ];

    /**
     * @var array
     */
    public $searchable = [
        'key',
    ];

    /**
     * @return array
     */
    public $rules = [
        'dateCreated' => 'required',
        'customerPONumber' => 'sometimes|max:20'
    ];

    /**************************************************************************
     * Relationships
     **************************************************************************/

    /**
    * @return Collection App\Models\OrderItem
    */
    public function skus()
    {
        return $this->hasMany(OrderItem::class, 'order_key', 'key');
    }

    /**
     * @return Collection App\Models\OrderLog
     */
    public function logs()
    {
        return $this->hasMany(OrderLog::class, 'order_key', 'key');
    }
}
