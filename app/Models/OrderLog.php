<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderLog extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'order_key',
        'externalKey',
        'errorMessage'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'id',
        'order_key',
        'errorMessage',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    public $searchable = [
        'externalKey'
    ];

    /**************************************************************************
     * Relationships
     **************************************************************************/

    /**
     * @return Collection App\Models\Order
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_key', 'key');
    }
}
