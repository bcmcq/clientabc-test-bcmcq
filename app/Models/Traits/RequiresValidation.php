<?php

namespace App\Models\Traits;

use App\Models\OrderLog;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

/**
 * Sets validation on a eloquent model. After the model is saved to the database,
 * validation will be ran and any invalid data will be logged to the order logs in
 * the database.
 *
 * This trait requires that you setup "$rules" on the model it is applied to.
 */
trait RequiresValidation
{
    /**
     * The RequiresValidation trait's "booting" method.
     *
     * @return void
     */
    public static function bootRequiresValidation()
    {
        self::saved(function ($model) {
            if (!isset($model->rules)) {
                abort(500, 'Validation $rules required on model '.get_class());
            }

            $validator = Validator::make($model->toArray(), $model->rules);

            if ($validator->fails()) {
                foreach ($validator->errors()->all() as $error) {
                    OrderLog::create([
                        'order_key' => $model->order_key ?? $model->key,
                        'errorMessage' => $error,
                        'externalKey' => $model->externalKey
                    ]);
                }

                return false;
            }
        });
    }
}
