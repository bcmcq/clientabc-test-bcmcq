<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'key' => $this->key,
            'externalKey' => $this->externalKey,
            'retailerCarrierServiceCode' => $this->retailerCarrierServiceCode,
            'salesChannel' => $this->salesChannel,
            'routingInstructions' => $this->routingInstructions,
            'customerPONumber' => $this->customerPONumber,
            'accountNumber' => $this->accountNumber,
            'carrierCode' => $this->carrierCode,
            'partner' => $this->partner,
            'shippingAddress1' => $this->shippingAddress1,
            'shippingAddress2' => $this->shippingAddress2,
            'shippingCity' => $this->shippingCity,
            'shippingState' => $this->shippingState,
            'deliveryName' => $this->deliveryName,
            'shippingCountry' => $this->shippingCountry,
            'shippingPostalCode' => $this->shippingPostalCode,
            'deliveryLocationCode' => $this->deliveryLocationCode,
            'billingAddress1' => $this->billingAddress1,
            'billingAddress2' => $this->billingAddress2,
            'billingCity' => $this->billingCity,
            'billingState' => $this->billingState,
            'billingCountry' => $this->billingCountry,
            'billingPostalCode' => $this->billingPostalCode,
            'externalReceivedDate' => $this->externalReceivedDate,
            'shipEndDate' => $this->shipEndDate,
            'shipStartDate' => $this->shipStartDate ?? $this->dateCreated ?? $this->orderDate,
            'orderDate' => $this->orderDate,
            'isTaxable' => $this->isTaxable,
            'dateCreated' => $this->dateCreated,
            'lastModified' => $this->updated_at->toDateTimeString(),
            'skus' => OrderItem::collection($this->whenLoaded('skus')),
            'logs' => OrderLog::collection($this->whenLoaded('logs')),
        ];
    }
}
