<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'externalKey' => $this->externalKey,
            'orderQuantity' => $this->orderQuantity,
            'unitPrice' => $this->unitPrice,
            'grossPrice' => $this->grossPrice,
            'UoM' => $this->UoM,
            'lineItemNo' => $this->lineItemNo,
            'order' => $this->order_key,
            'externalOrderId' => $this->externalOrderId,
            'sku' => $this->sku,
        ];
    }
}
