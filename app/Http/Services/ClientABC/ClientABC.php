<?php

namespace App\Http\Services\ClientABC;

use Illuminate\Support\Facades\Http;

/**
 * Service used to interact with Client ABC's web api.
 */
class ClientABC
{
    protected $domain;
    protected $ordersPath;
    protected $orderPath;

    /**
     * Create a new ClientABC service instance.
     */
    public function __construct()
    {
        $this->domain = config('services.clientabc.domain');
        $this->ordersPath = '/documents/order?statusList=New';
        $this->orderPath = '/documents/order/';
    }

    /**
     * Get all new orders
     *
     * @return json
     */
    public function getNewOrders()
    {
        $data = Http::get($this->domain . $this->ordersPath)->throw()->json();
        return $data['result'];
    }

    /**
     * Get a single order
     *
     * @param integer $id
     * @return json
     */
    public function getOrder(string $id)
    {
        return Http::get($this->domain . $this->orderPath . $id)->throw()->json();
    }
}
