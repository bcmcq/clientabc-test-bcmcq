<?php

namespace App\Http\Controllers;

use App\Models\OrderLog;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\OrderLog as Resource;

class OrderLogController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(OrderLog $orderLog)
    {
        return Resource::collection($this->runFilters($orderLog->query())->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(OrderLog $orderLog)
    {
        return new Resource($orderLog);
    }
}
