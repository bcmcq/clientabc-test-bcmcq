<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\Order as Resource;

class OrderController extends ApiController
{
    /**
     * Default Includes
     *
     * @var array
     */
    protected $includes = ['skus'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Order $order)
    {
        return Resource::collection($this->runFilters($order->query())->with($this->includes)->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $order->load($this->includes);

        return new Resource($order);
    }
}
