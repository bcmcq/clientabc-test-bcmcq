<?php

namespace App\Http\Controllers;

use App\Http\Filters\Filters;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * The data to be included with with the response.
     *
     * @var array
     */
    protected $includes = [];

    /**
     * Create a new Controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->includes = $this->parseIncludes();
    }

    /**
     * Parse includes for the resource.
     *
     * @return void
     */
    protected function parseIncludes()
    {
        $excludes = request()->get('exclude', []) ?? [];
        if (!is_array($excludes) && !empty($excludes)) {
            $excludes = explode(',', $excludes);
        }

        $includes = request()->get('include', []) ?? [];
        if (!is_array($includes) && !empty($includes)) {
            $includes = explode(',', $includes);
        }

        return array_filter(array_merge($includes, $this->includes), function ($include) use ($excludes) {
            return !in_array($include, $excludes);
        });
    }

    /**
     * Run query thought App\Http\Filters\Filters.
     *
     * @param object $query model query
     *
     * @return object model query
     */
    protected function runFilters($query)
    {
        return app(Pipeline::class)
            ->send($query)
            ->through(Filters::list())
            ->thenReturn();
    }
}
