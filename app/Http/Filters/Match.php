<?php

namespace App\Http\Filters;

use Illuminate\Support\Str;

/**
 * Searches for matches against database schema.
 * Can be either exact or wildcard via %.
 */
class Match extends Filter
{
    protected function applyFilter($builder)
    {
        $matches = request($this->filterName());

        if (! is_array($matches)) {
            return $builder;
        }

        foreach ($matches as $key => $value) {
            if (Str::contains($value, '%') !== false) {
                $builder->where($key, 'like', $value);
            } else {
                $builder->where($key, $value);
            }
        }

        return $builder;
    }
}
