<?php

namespace App\Http\Filters;

class Filters
{
    public static function list()
    {
        return [
            Match::class,
            Search::class,
            Sort::class,
        ];
    }
}
