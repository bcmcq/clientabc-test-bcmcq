<?php

namespace App\Http\Filters;

/**
 * Searches for wildcard match against schema items flagged as "searchable" on the model.
 */
class Search extends Filter
{
    protected function applyFilter($builder)
    {
        $terms = request($this->filterName());

        if (! is_array($terms)) {
            $terms = [$terms];
        }

        if (! $fields = $builder->getModel()->searchable) {
            return $builder;
        }

        foreach ($terms as $key => $term) {
            $builder->where(function ($query) use ($fields, $term) {
                foreach ($fields as $field) {
                    $query->orWhere($field, 'LIKE', "%$term%");
                }

                return $query;
            });
        }

        return $builder;
    }
}
