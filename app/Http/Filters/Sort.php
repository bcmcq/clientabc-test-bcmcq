<?php

namespace App\Http\Filters;

/**
 * Sort against schema fields via "asc" or "desc"
 */
class Sort extends Filter
{
    protected function applyFilter($builder)
    {
        $sortable = request($this->filterName());

        foreach ($sortable as $key => $value) {
            $builder->orderBy($key, $value);
        }

        return $builder;
    }
}
