<?php

namespace App\Observers;

use App\Models\Order;
use App\Observers\Traits\TransformDates;
use Illuminate\Support\Facades\Validator;

class OrderObserver
{
    use TransformDates;

    public function creating(Order $order)
    {
        $order->key = uniqid();
    }

    public function saving(Order $order)
    {
        $this->transformDatesToUTC($order, ['dateCreated', 'shipStartDate', 'shipEndDate', 'orderDate', 'externalReceivedDate']);
    }
}
