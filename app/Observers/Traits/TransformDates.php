<?php

namespace App\Observers\Traits;

use Illuminate\Support\Carbon;

/**
 * Transform dates to UTC
 */
trait TransformDates
{
    /**
     * Transform date formats to standard UTC
     * This doesn't do much as we don't know the incoming date format -- notes on that in README.md
     *
     * @param object $model
     * @param array $dates
     * @return void
     */
    public function transformDatesToUTC($model, $dates)
    {
        foreach ($dates as $date) {
            $dateValue = (string) $model->{$date};
            if (!empty($dateValue)) {
                $dateValue = Carbon::parse($dateValue);
                $model->{$date} = $dateValue->tz('UTC')->toDateTimeString();
            }
        }
    }
}
