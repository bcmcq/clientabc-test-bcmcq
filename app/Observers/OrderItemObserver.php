<?php

namespace App\Observers;

use App\Models\Order;
use App\Models\OrderItem;

class OrderItemObserver
{
    /**
     * Handle the order item "created" event.
     *
     * @param  \App\OrderItem  $orderItem
     * @return void
     */
    public function creating(OrderItem $orderItem)
    {
        if (is_null($orderItem->order_key)) {
            $order = Order::where('externalKey', $orderItem->externalKey)->first();
            if ($order) {
                $orderItem->order_key = $order->key;
            }
        }
    }

    public function saving(OrderItem $orderItem)
    {
        $orderItem->grossPrice = $orderItem->unitPrice * $orderItem->orderQuantity;
    }
}
