# Notes

## General Thoughts & Observations

When first approaching the integration job (4th step - Appendix J), I first assumed that we were to write a [Laravel Queue Job](https://laravel.com/docs/7.x/queues#creating-jobs) to handle the task. After reading again and observing that no link to the Laravel docs regarding queues/jobs was provided in the "References" section I came to the assumption we were to just build out the code in the seeder, so this is the approach I took.

## Dates & Timestamps

- I couldn't figure out the whole timestamps situation. My first assumption was the time may be seperated from the dates so I was looking for where that may be located. My second assumption was the times are being rolled over to UTC from a specific timezone but being as the example incoming date were dates only with no timestamps, that didn't match up with the example output either. My final conclusion was that the actual dates were deleted and integer representations of dates were put in their place just to see how we would handle it.

- I made a few fallbacks for some dates in the resource output simply because they seemed to overlap. I also added the date transformations to the order model just for this test. In a production situation, handling data from all over the place, I would assume that the date transformation should probably be moved to the importe layer; A timezone lookup api or database be integrated to detect incoming timezone based on address / location data and transform to UTC format. Then adding all dates to the $dates array on the eloquent models to utilize Laravel's eloquent date abilities w/ Carbon.

## Database Storage

- The current setup would write new orders each time regardless of if they were actually *new*. I would assume that `/documents/order?statusList=New` doesn't always return orders that we've never seen. I tried to keep the database reading and writing to a minimum regardless. Production would probably want to have some import status or other method of tracking already processed orders unless the data is constantly changing and importing everytime is desired. This could potentially be a bottleneck as the system grows.

- Currently, the way the seeder is setup if a request for an individual order failed it could potentially skip that order (it could get lost). Just wanted to point that out, I'm aware but didn't feel it was necessary to address given that this is just a test.

- I decided to write all incoming data to the database on orders and order items regardless of validation passes. This is based on the output example of order_logs. I noticed that the order_logs all have order #'s (`"order": "5ebb05d0ecde8",`) based on the `uniqid()` which leads me to assume they've been written to the database. I wasn't sure this was correct but given the examples provided, this is what I went with.

## Includes & Excludes

Order items (skus) are included on the orders by default. You may exclude them to shorten returned payload or you may also include logs.

### Includes

```/orders?include=skus,logs```

### Excludes

```/orders?exclude=skus```

## Filters

Below is a description of currently available filters. All filters can be used more than once and will apply in the order written.

### Match

The match filter will search the database for all matching values for a specific field. Wildcard is also available via `%`.

*Examples*
```
/orders?match[key]=5ed43a926074e
/orders?match[key]=5%
/orders?match[key]=5%&match[salesChannel]=EDI
```

### Search

The search filter will search the database aginst the *searchable* fields on the given model. Searchable fields are defined on the model via the `$searchable` array. Search is wildcard by default, it'll attempt to find the given value anywhere in the searched data.

*Examples*
```
/orders?search=5ed43a926074e
/orders?search=5
/orders?search=5&search=0
```

### Sort

The sort filter will allow you to sort returned data by a given field in `asc` or `desc` order.

*Examples*
```
/orders?sort[shippingCity]=asc
/orders?sortsort[shippingCity]=desc
/orders?sort[carrierCode]=asc&sort[shippingCity]=desc
```
