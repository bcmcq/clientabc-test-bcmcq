<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use App\Models\Order;
use App\Models\OrderLog;
use App\Models\OrderItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function order_database_table_has_expected_columns()
    {
        $this->assertTrue(
            Schema::hasColumns('orders', [
                'id',
                'key',
                'externalKey',
                'retailerCarrierServiceCode',
                'salesChannel',
                'routingInstructions',
                'customerPONumber',
                'accountNumber',
                'carrierCode',
                'freightAmount',
                'partner',
                'deliveryLocationCode',
                'billingAddress1',
                'billingAddress2',
                'billingCity',
                'billingState',
                'billingPostalCode',
                'billingCountry',
                'deliveryName',
                'shippingAddress1',
                'shippingAddress2',
                'shippingCity',
                'shippingState',
                'shippingPostalCode',
                'shippingCountry',
                'isTaxable',
                'externalReceivedDate',
                'shipStartDate',
                'shipEndDate',
                'dateCreated',
                'orderDate',
                'created_at',
                'updated_at'
            ]),
            true
        );
    }

    /** @test */
    public function order_can_be_created_and_read()
    {
        $order = factory(Order::class)->create();
        $this->assertDatabaseHas('orders', ['key' => $order->key]);
    }

    /** @test */
    public function order_can_be_updated()
    {
        $order = factory(Order::class)->create()->update(['shippingAddress1' => '123 Some St.']);
        $this->assertDatabaseHas('orders', ['shippingAddress1' => '123 Some St.']);
    }

    /** @test */
    public function order_can_be_deleted()
    {
        $order = factory(Order::class)->create();
        $key = $order->key;
        $order->delete();
        $this->assertDeleted('orders', ['key' => $key]);
    }

    /** @test */
    public function order_has_many_order_items()
    {
        $order = factory(Order::class)->create();
        $item = factory(OrderItem::class)->create(['order_key' => $order->key]);
        $order->load('skus');

        $this->assertTrue($order->skus->contains($item));
    }

    /** @test */
    public function order_has_many_order_logs()
    {
        $order = factory(Order::class)->create();
        $log = factory(OrderLog::class)->create(['order_key' => $order->key]);
        $order->load('logs');

        $this->assertTrue($order->logs->contains($log));
    }

    /** @test */
    public function order_validates_data()
    {
        $order = new Order;
        $order->externalKey = 123;
        $order->customerPONumber = 'A999999999999999999999999999999999';
        $order->save();

        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The date created field is required.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The customer p o number may not be greater than 20 characters.']);
    }
}
