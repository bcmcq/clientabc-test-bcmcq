<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use App\Models\Order;
use App\Models\OrderLog;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OrderLogTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function order_logs_database_table_has_expected_columns()
    {
        $this->assertTrue(
            Schema::hasColumns('order_logs', [
                'order_key',
                'externalKey',
                'errorMessage',
                'created_at',
                'updated_at'
            ]),
            true
        );
    }

    /** @test */
    public function order_logs_can_be_created_and_read()
    {
        $log = factory(OrderLog::class)->create();
        $this->assertDatabaseHas('order_logs', ['id' => $log->id]);
    }

    /** @test */
    public function order_logs_can_be_updated()
    {
        $log = factory(OrderLog::class)->create()->update(['errorMessage' => 'unit test msg']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'unit test msg']);
    }

    /** @test */
    public function order_logs_can_be_deleted()
    {
        $log = factory(OrderLog::class)->create();
        $id = $log->id;
        $log->delete();
        $this->assertDeleted('order_logs', ['id' => $id]);
    }

    /** @test */
    public function order_logs_belongs_to_order()
    {
        $order = factory(Order::class)->create();
        $log = factory(OrderLog::class)->create(['order_key' => $order->key]);

        $this->assertInstanceOf(Order::class, $log->order);
    }
}
