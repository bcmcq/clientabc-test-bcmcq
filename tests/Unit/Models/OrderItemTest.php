<?php

namespace Tests\Unit\Models;

use Tests\TestCase;
use App\Models\Order;
use App\Models\OrderLog;
use App\Models\OrderItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OrderItemTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function order_items_database_table_has_expected_columns()
    {
        $this->assertTrue(
            Schema::hasColumns('order_items', [
                'id',
                'order_key',
                'sku',
                'lineItemNo',
                'UoM',
                'orderQuantity',
                'unitPrice',
                'grossPrice',
                'externalOrderId',
                'created_at',
                'updated_at'
            ]),
            true
        );
    }

    /** @test */
    public function order_items_can_be_created_and_read()
    {
        $item = factory(OrderItem::class)->create();
        $this->assertDatabaseHas('order_items', ['id' => $item->id]);
    }

    /** @test */
    public function order_items_can_be_updated()
    {
        $item = factory(OrderItem::class)->create()->update(['sku' => 'sku-test-01']);
        $this->assertDatabaseHas('order_items', ['sku' => 'sku-test-01']);
    }

    /** @test */
    public function order_items_can_be_deleted()
    {
        $item = factory(OrderItem::class)->create();
        $id = $item->id;
        $item->delete();
        $this->assertDeleted('order_items', ['id' => $id]);
    }

    /** @test */
    public function order_items_belongs_to_order()
    {
        $order = factory(Order::class)->create();
        $item = factory(OrderItem::class)->create(['order_key' => $order->key]);

        $this->assertInstanceOf(Order::class, $item->order);
    }

    /** @test */
    public function order_item_validates_data()
    {
        $orderItem = new OrderItem;
        $orderItem->order_key = 123;
        $orderItem->save();

        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The sku field is required.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The line item no field is required.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The uo m field is required.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The order quantity field is required.']);

        $orderItem->sku = '12345678901234567890';
        $orderItem->lineItemNo = '123456789012345678901234567890123456789012345678901234567890';
        $orderItem->UoM = '123456789012345678901234567890123456789012345678901234567890';
        $orderItem->orderQuantity = 123456789099;
        $orderItem->unitPrice = '123456789012345678901234567890';
        $orderItem->externalOrderId = '123456789012345678901234567890';
        $orderItem->save();

        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The sku may not be greater than 15 characters.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The line item no may not be greater than 50 characters.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The uo m may not be greater than 50 characters.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The order quantity may not be greater than 10 characters.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The unit price may not be greater than 10 characters.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The gross price may not be greater than 10 characters.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The external order id may not be greater than 15 characters.']);
        $this->assertDatabaseHas('order_logs', ['errorMessage' => 'The external key may not be greater than 25 characters.']);
    }
}
