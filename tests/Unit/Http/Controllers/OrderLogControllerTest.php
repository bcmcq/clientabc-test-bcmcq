<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;
use App\Models\OrderLog;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OrderLogControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_return_list_of_order_logs()
    {
        $orderLogs = $this->createOrderLogs(2);
        $response = $this->get('/orderLogs');

        $this->assertEquals($orderLogs->jsonSerialize(), $response->json());
    }

    /** @test */
    public function can_return_single_order_log()
    {
        $orderLog = $this->createOrderLogs();
        $response = $this->get("/orderLogs/{$orderLog->id}");

        $this->assertEquals($orderLog->jsonSerialize(), $response->json());
    }

    /** @test */
    public function can_filter_order_logs_by_schema_data()
    {
        $orderLog = $this->createOrderLogs();
        $response = $this->get("/orderLogs?match[externalKey]={$orderLog->externalKey}");

        $this->assertEquals([$orderLog->jsonSerialize()], $response->json());
    }

    /** @test */
    public function can_search_order_logs_by_searchable_fields()
    {
        $orderLog = $this->createOrderLogs();
        $response = $this->get("/orderLogs?search={$orderLog->externalKey}");

        $this->assertEquals([$orderLog->jsonSerialize()], $response->json());
    }

    /** @test */
    public function can_sort_order_logs_by_schema_data()
    {
        $orderLogs = $this->createOrderLogs(2);
        $response1 = $this->get('/orderLogs?sort[id]=asc')->json();
        $response2 = $this->get('/orderLogs?sort[id]=desc')->json();

        $this->assertNotEquals($response1, $response2);
    }

    /**
     * Create order log helper
     *
     * @param integer $count
     */
    private function createOrderLogs($count = 1)
    {
        if ($count > 1) {
            $orderLogs = factory(OrderLog::class, $count)->create();
            foreach ($orderLogs as $orderLog) {
                $orderLog->order = $orderLog->order_key;
                $orderLog->message = $orderLog->errorMessage;
            }
        } else {
            $orderLogs = factory(OrderLog::class)->create();
            $orderLogs->order = $orderLogs->order_key;
            $orderLogs->message = $orderLogs->errorMessage;
        }

        return $orderLogs;
    }
}
