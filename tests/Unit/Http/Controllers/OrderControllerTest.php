<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;
use App\Models\Order;
use App\Models\OrderLog;
use App\Models\OrderItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OrderControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_return_list_of_orders()
    {
        $orders = $this->createOrders(2);
        $response = $this->get('/orders?exclude=skus');

        $this->assertEquals($orders->jsonSerialize(), $response->json());
    }

    /** @test */
    public function can_return_single_order()
    {
        $order = $this->createOrders();
        $response = $this->get("/orders/{$order->key}?exclude=skus");

        $this->assertEquals($order->jsonSerialize(), $response->json());
    }

    /** @test */
    public function can_return_default_relationship_skus_on_order()
    {
        $order = $this->createOrders();
        $orderItem = factory(OrderItem::class)->create(['order_key' => $order->key]);
        $response = $this->get("/orders/{$order->key}");

        $this->assertArrayHasKey('skus', $response->json());
    }

    /** @test */
    public function can_exclude_relationships_on_order()
    {
        $order = $this->createOrders();
        $orderItem = factory(OrderItem::class)->create(['order_key' => $order->key]);
        $response = $this->get("/orders/{$order->key}?exclude=skus");

        $this->assertArrayNotHasKey('skus', $response->json());
    }

    /** @test */
    public function can_included_relationships_on_order()
    {
        $order = $this->createOrders();
        $orderItem = factory(OrderItem::class)->create(['order_key' => $order->key]);
        $orderLog = factory(OrderLog::class)->create(['order_key' => $order->key]);
        $response = $this->get("/orders/{$order->key}?include=skus,logs");

        $this->assertEquals(1, count($response->json()['skus']));
        $this->assertEquals(1, count($response->json()['logs']));
    }

    /** @test */
    public function can_filter_orders_by_schema_data()
    {
        $order = $this->createOrders();
        $response = $this->get("/orders?exclude=skus&match[externalKey]={$order->externalKey}");

        $this->assertEquals([$order->jsonSerialize()], $response->json());
    }

    /** @test */
    public function can_search_orders_by_searchable_fields()
    {
        $order = $this->createOrders();
        $response = $this->get("/orders?exclude=skus&search={$order->key}");

        $this->assertEquals([$order->jsonSerialize()], $response->json());
    }

    /** @test */
    public function can_sort_orders_by_schema_data()
    {
        $orders = $this->createOrders(2);
        $response = $this->get('/orders?exclude=skus&sort[id]=desc');

        $this->assertEquals(2, $response[0]['id']);
    }

    /**
     * Create order helper
     *
     * @param integer $count
     */
    private function createOrders($count = 1)
    {
        if ($count > 1) {
            $orders = factory(Order::class, $count)->create();
            foreach ($orders as $order) {
                $order->lastModified = $order->updated_at;
            }
        } else {
            $orders = factory(Order::class)->create();
            $orders->lastModified = $orders->updated_at;
        }

        return $orders;
    }
}
