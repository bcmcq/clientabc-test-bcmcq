<?php

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;
use App\Models\OrderItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class OrderItemControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_return_list_of_order_items()
    {
        $orderItems = $this->createOrderItems(2);
        $response = $this->get('/orderItems');

        $this->assertEquals(2, count($response->json()));
    }

    /** @test */
    public function can_return_single_order_item()
    {
        $orderItem = $this->createOrderItems();
        $response = $this->get("/orderItems/{$orderItem->id}");

        $this->assertEquals($orderItem->jsonSerialize(), $response->json());
    }

    /** @test */
    public function can_filter_order_items_by_schema_data()
    {
        $orderItem = $this->createOrderItems();
        $response = $this->get("/orderItems?match[sku]={$orderItem->sku}");

        $this->assertEquals([$orderItem->jsonSerialize()], $response->json());
    }

    /** @test */
    public function can_search_order_items_by_searchable_fields()
    {
        $orderItem = $this->createOrderItems();
        $response = $this->get("/orderItems?search={$orderItem->sku}");

        $this->assertEquals([$orderItem->jsonSerialize()], $response->json());
    }

    /** @test */
    public function can_sort_order_items_by_schema_data()
    {
        $orderItem = $this->createOrderItems(2);
        $response = $this->get('/orderItems?sort[id]=desc');

        $this->assertEquals(2, $response[0]['id']);
    }

    /**
     * Create order item helper
     *
     * @param integer $count
     */
    private function createOrderItems($count = 1)
    {
        if ($count > 1) {
            $orderItems = factory(OrderItem::class, $count)->create();
            foreach ($orderItems as $orderItem) {
                $orderItem->order = $orderItem->order_key;
            }
        } else {
            $orderItems = factory(OrderItem::class)->create();
            $orderItems->order = $orderItems->order_key;
        }

        return $orderItems;
    }
}
