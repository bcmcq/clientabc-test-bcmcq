<?php

namespace Tests\Unit\Imports\ClientABC;

use Tests\TestCase;
use App\Models\OrderItem;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Http\Services\ClientABC\ClientABC;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Imports\ClientABC\OrderItemTransformer as Transform;

class OrderItemTransformerTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function order_item_transformer_has_data_map()
    {
        $order = json_decode(File::get(base_path('tests/Stubs/Services/ClientABC/order_40721314.json')), true);
        $dataMap = (new Transform($order['items'][0], ['order_key' => 123]))->getDataMap();

        $this->assertGreaterThan(0, count($dataMap));
    }

    /** @test  */
    public function order_item_transformer_transforms_data_and_saves_to_database()
    {
        $order = json_decode(File::get(base_path('tests/Stubs/Services/ClientABC/order_40721314.json')), true);
        $orderItem = (new Transform($order['items'][0], ['order_key' => $order['recordId']]))->transform();

        $this->assertDatabaseHas('order_items', ['id' => $orderItem->id]);
    }
}
