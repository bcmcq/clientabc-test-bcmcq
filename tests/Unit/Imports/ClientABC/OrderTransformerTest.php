<?php

namespace Tests\Unit\Imports\ClientABC;

use Tests\TestCase;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Http\Services\ClientABC\ClientABC;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Imports\ClientABC\OrderTransformer as Transform;

class OrderTransformerTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function order_transformer_has_data_map()
    {
        $order = json_decode(File::get(base_path('tests/Stubs/Services/ClientABC/order_40721314.json')), true);

        $dataMap = (new Transform($order))->getDataMap();
        $this->assertGreaterThan(0, count($dataMap));
    }

    /** @test  */
    public function order_transformer_transforms_data_and_saves_to_database()
    {
        $order = json_decode(File::get(base_path('tests/Stubs/Services/ClientABC/order_40721314.json')), true);
        $order = (new Transform($order))->transform();

        $this->assertDatabaseHas('orders', ['key' => $order->key]);
    }

    /** @test  */
    public function order_transformer_standardizes_order_data()
    {
        $order = json_decode(File::get(base_path('tests/Stubs/Services/ClientABC/order_40721325.json')), true);
        $order = (new Transform($order));
        $order->standardizeOrderData();

        $this->assertArrayHasKey('shipToAddress', $order->getIncomingData());
    }
}
