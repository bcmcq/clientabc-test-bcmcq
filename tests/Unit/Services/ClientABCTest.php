<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Http\Services\ClientABC\ClientABC;

class ClientABCTest extends TestCase
{
    /** @test */
    public function client_abc_get_new_orders_request_returns_data()
    {
        Http::fake([
            '*' => Http::response(File::get(base_path('tests/Stubs/Services/ClientABC/orders.json')), 200, ['Headers']),
        ]);

        $orders = (new ClientABC)->getNewOrders();
        $this->assertIsArray($orders);
    }

    /** @test */
    public function client_abc_get_single_order_request_returns_data()
    {
        Http::fake([
            '*' => Http::response(File::get(base_path('tests/Stubs/Services/ClientABC/order_40721314.json')), 200, ['Headers']),
        ]);

        $order = (new ClientABC)->getOrder('40721314');
        $this->assertCount(91, $order);
    }
}
