<?php

use Illuminate\Database\Seeder;
use App\Http\Services\ClientABC\ClientABC;
use App\Imports\ClientABC\OrderTransformer;
use App\Imports\ClientABC\OrderItemTransformer;

class ClientABCSeeder extends Seeder
{
    protected $httpClien;

    public function __construct(ClientABC $client)
    {
        $this->httpClient = $client;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Http::fake([
            '*/order?statusList=New' => Http::response(File::get(base_path('tests/Stubs/Services/ClientABC/orders.json')), 200, ['Headers']),
            '*/order/40721314' => Http::response(File::get(base_path('tests/Stubs/Services/ClientABC/order_40721314.json')), 200, ['Headers']),
            '*/order/40721325' => Http::response(File::get(base_path('tests/Stubs/Services/ClientABC/order_40721325.json')), 200, ['Headers']),
        ]);

        $incomingOrderData = $this->httpClient->getNewOrders();

        $orders = [];

        foreach ($incomingOrderData as $orderData) {
            array_push($orders, $orderData['documentId']);
        }

        foreach ($orders as $order) {
            $orderData = $this->httpClient->getOrder($order);

            $orderTransformer = new OrderTransformer($orderData);
            $order = $orderTransformer->transform();

            if (count($orderData['items'])) {
                foreach ($orderData['items'] as $orderItem) {
                    $preAssignedData = [
                        'order_key' => $order->key,
                        'externalOrderId' => $order->externalKey
                    ];

                    $orderItemTransformer = new OrderItemTransformer($orderItem, $preAssignedData);
                    $orderItemTransformer->transform();
                }
            }
        }
    }
}
