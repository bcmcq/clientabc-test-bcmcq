<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->string('order_key')->index()->nullable();
            $table->string('sku', 15)->nullable();
            $table->string('lineItemNo', 50)->nullable();
            $table->string('UoM', 50)->default('EA');
            $table->integer('orderQuantity')->unsigned()->nullable();
            $table->float('unitPrice', 10, 2)->unsigned()->nullable();
            $table->float('grossPrice', 10, 2)->unsigned()->nullable();
            $table->string('externalOrderId', 25)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
