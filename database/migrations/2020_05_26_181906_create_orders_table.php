<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('key')->unique();
            $table->string('externalKey');
            $table->string('retailerCarrierServiceCode')->nullable();
            $table->string('salesChannel')->nullable();
            $table->string('routingInstructions')->nullable();
            $table->string('customerPONumber', 20)->nullable();
            $table->string('accountNumber')->nullable();
            $table->string('carrierCode')->nullable();
            $table->float('freightAmount', 10, 2)->unsigned()->nullable();
            $table->string('partner')->nullable();
            $table->string('deliveryLocationCode')->nullable();
            $table->string('billingAddress1')->nullable();
            $table->string('billingAddress2')->nullable();
            $table->string('billingCity')->nullable();
            $table->string('billingState')->nullable();
            $table->string('billingPostalCode')->nullable();
            $table->string('billingCountry')->nullable();
            $table->string('deliveryName')->nullable();
            $table->string('shippingAddress1')->nullable();
            $table->string('shippingAddress2')->nullable();
            $table->string('shippingCity')->nullable();
            $table->string('shippingState')->nullable();
            $table->string('shippingPostalCode')->nullable();
            $table->string('shippingCountry')->nullable();
            $table->boolean('isTaxable')->default(true);
            $table->timestamp('externalReceivedDate')->nullable();
            $table->timestamp('shipStartDate')->nullable();
            $table->timestamp('shipEndDate')->nullable();
            $table->timestamp('dateCreated')->nullable();
            $table->timestamp('orderDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
