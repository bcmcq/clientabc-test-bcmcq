<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Order;
use App\Models\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    $qty = $faker->numberBetween(1, 7);
    $price = $faker->randomFloat(2, 20, 100);

    return [
        'order_key' => $faker->word,
        'sku' => $faker->word,
        'lineItemNo' => $faker->word,
        'UoM' => $faker->word,
        'orderQuantity' => $qty,
        'unitPrice' => $price,
        'grossPrice' => $price * $qty,
        'externalOrderId' => $faker->word,
    ];
});
