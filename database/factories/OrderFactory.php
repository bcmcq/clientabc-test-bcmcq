<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'key' => uniqid(),
        'externalKey' => $faker->shuffle('abcd1234'),
        'retailerCarrierServiceCode' => $faker->sentence,
        'salesChannel' => $faker->catchPhrase,
        'routingInstructions' => $faker->sentence,
        'customerPONumber' => (string) $faker->randomNumber(),
        'accountNumber' => (string) $faker->randomNumber(),
        'carrierCode' => $faker->randomElement(['FDX', 'UPS']),
        'partner' => $faker->company,
        'deliveryLocationCode' => $faker->word,
        'billingAddress1' => $faker->streetAddress,
        'billingAddress2' => $faker->secondaryAddress,
        'billingCity' => $faker->city,
        'billingState' => $faker->stateAbbr,
        'billingPostalCode' => $faker->postcode,
        'billingCountry' => $faker->country,
        'deliveryName' => $faker->company,
        'shippingAddress1' => $faker->streetAddress,
        'shippingAddress2' => $faker->secondaryAddress,
        'shippingCity' => $faker->city,
        'shippingState' => $faker->stateAbbr,
        'shippingPostalCode' => $faker->postcode,
        'shippingCountry' => $faker->country,
        'isTaxable' => (int) $faker->boolean,
        'externalReceivedDate' => $faker->iso8601(),
        'shipEndDate' => $faker->iso8601(),
        'shipStartDate' => $faker->iso8601(),
        'dateCreated' => $faker->iso8601(),
        'orderDate' => $faker->iso8601(),
    ];
});
