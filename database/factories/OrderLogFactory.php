<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Order;
use App\Models\OrderLog;
use Faker\Generator as Faker;

$factory->define(OrderLog::class, function (Faker $faker) {
    return [
        'order_key' => $faker->word,
        'externalKey' => $faker->word,
        'errorMessage' => $faker->word
    ];
});
